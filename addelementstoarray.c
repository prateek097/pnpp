

#include <stdio.h>
int main()
{
    int arr[50];
    int i, size, num, pos;
    printf("Enter size of the array\n");
    scanf("%d", &size);
    printf("Enter elements in array\n");
    for(i=0; i<size; i++)
    {
        scanf("%d", &arr[i]);
    }
    printf("Enter element to insert\n");
    scanf("%d", &num);
    printf("Enter the element position\n");
    scanf("%d", &pos);
    if(pos > size+1 || pos <= 0)
    {
        printf("Invalid position! Please enter position between 1 to %d", size);
    }
    else
    {
        for(i=size; i>=pos; i--)
        {
            arr[i] = arr[i-1];
        }
        arr[pos-1] = num;
        size++;       
        printf("Array elements after insertion\n");
        for(i=0; i<size; i++)
        {
            printf("%d\n", arr[i]);
        }
    }

    return 0;
}